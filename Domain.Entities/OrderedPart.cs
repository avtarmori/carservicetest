﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
   public class OrderedPart:BaseEntity
    {
       
        public int Quantity { get; set; }
        public Part Part { get; set; }

        public int PartId { get; set; }
        public int OrderId { get; set; }
    }
}
