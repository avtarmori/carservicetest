﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Car:BaseEntity
    {
        public int ModelId { get; set; }
        public Model Model { get; set; }
        public string RegistrationNumber { get; set; }
        public string VehicleIdentificationNumber { get; set; }
      

        public List<CustomerCars> CustomerCars { get; set; }
    }
}