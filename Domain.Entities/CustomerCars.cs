﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class CustomerCars 

    {
        public int Id { get; set; }
        public int CarId { get; set; }
        public int CustomerId { get; set; }

        public Car Car { get; set; }
        public Customer Customer { get; set; }
    }
}