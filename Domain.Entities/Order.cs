﻿using System.Collections.Generic;

namespace Domain.Entities
{
    public class Order : BaseEntity
    {
        public Enums.OrderStatus Status { get; set; }
        public int OdometerReading { get; set; }
        public short FuelPercent { get; set; }
        public string CustomerComment { get; set; }
        public int Amount { get; set; }
        public int CustomerId { get; set; }
        public int CarId { get; set; }
        public List<OrderedService> OrderedServices { get; set; }
        public List<OrderedPart> OrderedParts { get; set; }
        public Customer Customer { get; set; }
        public Car Car { get; set; }
    }
}