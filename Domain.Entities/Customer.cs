﻿
using System.Collections.Generic;

namespace Domain.Entities
{
    public class Customer: BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal PhoneNumber { get; set; }
        public string EmailId { get; set; }

       
        public List<CustomerCars> CustomerCars { get; set; }
    }
}
