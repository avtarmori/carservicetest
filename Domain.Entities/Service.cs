﻿namespace Domain.Entities
{
    public class Service:BaseEntity
    {

        public string Name { get; set; }
       
        public int Amount { get; set; }
        public short TaxPercentage { get; set; }

    }
}