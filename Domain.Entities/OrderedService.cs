﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
   public class OrderedService:BaseEntity
    {
       
        public int Quantity { get; set; }
        public Service Service { get; set; }

        public int ServiceId { get; set; }
        public int OrderId { get; set; }

    }
}
