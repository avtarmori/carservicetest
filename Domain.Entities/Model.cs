﻿namespace Domain.Entities
{
    public class Model : BaseEntity
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public int CC { get; set; }
        public bool AutoTransmission { get; set; }
        public bool AC { get; set; }
        public string FullName { get; set; }

        public Brand Brand { get; set; }
    }
}