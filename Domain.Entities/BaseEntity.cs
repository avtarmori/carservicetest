﻿using System;
namespace Domain.Entities
{
    public class BaseEntity
    {
        public  BaseEntity()
        {
            CreatedOn = DateTime.Now;
        }
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}