﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.DataFilters
{
   public class OrderFilter
    {
        public int? OrderId { get; set; }
        public int? CustomerId { get; set; }
        public int? CarId { get; set; }
    }
}
