﻿
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
   public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll();
        T Get(long id);
        int Insert(T entity);
      
        void Update(T entity);
        void Delete(long id);

        int UOW_Insert(T entity);

        void UOW_Update(T entity);
        void UOW_Delete(long id);

        IEnumerable<T> Get(
          Expression<Func<T, bool>> filter = null,
          Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
          string includeProperties = "");

        IEnumerable<T> GetPage(out int totalCount, int? page = null, int? itemsPerPage = null,
           Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "");
    }
}
