﻿using Domain.Entities;
using System.Collections.Generic;

namespace Domain.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
         List<OrderedService> GetOrderedServices(int OrderId);
        List<OrderedPart> GetOrderedParts(int OrderId);

    }
}
