﻿// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function InitializeValidationOnPartialPostback() {
    $("form").each(function () { $.data($(this)[0], 'validator', false); });
    $.validator.unobtrusive.parse("form");

}

$(function () {
    $('.typeahead').on('keyup', function (e) {
        if (e.keyCode == 13) {
            var ta = $(this).data('typeahead');
            var val = ta.$menu.find('.active').data('value');
            if (val || $(this).val()=='')
            $('form').submit();
        }
    });
});