﻿using Web.MVC.Models;
using Domain.Entities;
using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Infrastructure.DataFilters;

namespace Web.MVC.Controllers
{
    public class OrderController : Controller
    {
       

        private readonly IOrderService orderService;

        public OrderController(IOrderService _orderService)
        {
            orderService = _orderService;
        }
        public ActionResult Index(int? page, OrderHistoryViewModel _OrderHistoryViewModel, string PhoneNumber, string RegistrationNumber)
        {
            ViewBag.PhoneNumber = PhoneNumber;
            ViewBag.RegistrationNumber = RegistrationNumber;

            int pagenumber = (page ?? 1) - 1; //I know what you're thinking, don't put it on 0 :)
                                              // OrderManagement orderMan = new OrderManagement(HttpContext.ApplicationInstance.Context);
            int totalCount = 0;
            IEnumerable<Order> orders = orderService.GetOrderPage(page, Settings.DefaultPageSize, new OrderFilter() { CarId = _OrderHistoryViewModel.CarId, CustomerId = _OrderHistoryViewModel.CustomerId, OrderId = _OrderHistoryViewModel.OrderId }, out totalCount);
            //List<OrderViewModel> orderViews = new List<OrderViewModel>();
            //foreach (Order order in orders)//convert your models to some view models.
            //{
            //    orderViews.Add(orderService.GenerateOrderViewModel(order));
            //}
            //create staticPageList, defining your viewModel, current page, page size and total number of pages.
            IPagedList<Order> pageOrders = new StaticPagedList<Order>(orders, pagenumber + 1, Settings.DefaultPageSize, totalCount);

            _OrderHistoryViewModel = _OrderHistoryViewModel ?? new OrderHistoryViewModel();
            _OrderHistoryViewModel.Orders = pageOrders;
            return View(_OrderHistoryViewModel);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(CreateOrderViewModel createOrderViewModel)
        {

            int orderId = orderService.Create(new Order()
            {
                CustomerId = createOrderViewModel.CustomerId ?? 0
                  ,
                Customer = createOrderViewModel.CustomerId.HasValue ? null : new Customer()
                {
                    FirstName = createOrderViewModel.FirstName,
                    LastName = createOrderViewModel.LastName,
                    PhoneNumber = createOrderViewModel.PhoneNumber,
                    EmailId = createOrderViewModel.EmailId
                }
                  ,
                CarId = createOrderViewModel.CarId ?? 0
                  ,
                Car = createOrderViewModel.CarId.HasValue ? null : new Car() { RegistrationNumber = createOrderViewModel.RegistrationNumber, VehicleIdentificationNumber = createOrderViewModel.VehicleIdentificationNumber, ModelId = createOrderViewModel.ModelId }
                ,
                CustomerComment = createOrderViewModel.CustomerComment
                  ,
                FuelPercent = createOrderViewModel.FuelPercent
                  ,
                OdometerReading = createOrderViewModel.OdometerReading

            });
            return RedirectToAction("AddServices", new { OrderId = orderId });
        }

        public ActionResult AddServices(int OrderId)
        {

            AddServicesViewModel _AddServicesViewModel = new AddServicesViewModel();

            _AddServicesViewModel.AddedServices = GetOrderedServices(OrderId);

            return View(_AddServicesViewModel);
        }
        [HttpPost]
        public ActionResult AddServices(AddServicesViewModel _AddServicesViewModel, int OrderId)
        {
            if (_AddServicesViewModel.AddedServices != null)
                orderService.UpdateServices(_AddServicesViewModel.AddedServices.Select(s => new OrderedService() { Id = s.Id, Quantity = s.Quantity }));

            if (_AddServicesViewModel.SelectedServiceId != null)
                orderService.AddService(_AddServicesViewModel.SelectedServiceId.Value, OrderId);
            else if (_AddServicesViewModel.DeleteOrderedServiceId != null)
                orderService.RemoveService(_AddServicesViewModel.DeleteOrderedServiceId.Value);
            else
                return JavaScript("window.location = '" + Url.Action("AddParts", new { OrderId = OrderId }) + "'");


            ModelState.Clear();
            _AddServicesViewModel.SelectedServiceId = null;
            _AddServicesViewModel.DeleteOrderedServiceId = null;

            _AddServicesViewModel.AddedServices = GetOrderedServices(OrderId);

            return PartialView("_AddedServices", _AddServicesViewModel);
        }
        private List<AddServiceViewModel> GetOrderedServices(int OrderId)
        {
            var orderedServices = orderService.GetOrderedServices(OrderId);
            if (orderedServices != null)
                return orderedServices.Select(s => new AddServiceViewModel() { Id = s.Id, Name = s.Service.Name, Quantity = s.Quantity, Amount = s.Service.Amount }).ToList();
            else
                return new List<AddServiceViewModel>();
        }


        public ActionResult AddParts(int OrderId)
        {

            AddPartsViewModel _AddPartsViewModel = new AddPartsViewModel();

            _AddPartsViewModel.AddedParts = GetOrderedParts(OrderId);

            return View(_AddPartsViewModel);
        }
        [HttpPost]
        public ActionResult AddParts(AddPartsViewModel _AddPartsViewModel, int OrderId)
        {
            if (_AddPartsViewModel.AddedParts != null)
                orderService.UpdateParts(_AddPartsViewModel.AddedParts.Select(s => new OrderedPart() { Id = s.Id, Quantity = s.Quantity }));

            if (_AddPartsViewModel.SelectedPartId != null)
            {
                orderService.AddPart(_AddPartsViewModel.SelectedPartId.Value, OrderId);
            }
            else if (_AddPartsViewModel.DeleteOrderedPartId != null)
            {
                orderService.RemovePart(_AddPartsViewModel.DeleteOrderedPartId.Value);
            }
            else
                return JavaScript("window.location = '" + Url.Action("Index") + "'");


            ModelState.Clear();
            _AddPartsViewModel.SelectedPartId = null;
            _AddPartsViewModel.DeleteOrderedPartId = null;

            _AddPartsViewModel.AddedParts = GetOrderedParts(OrderId);

            return PartialView("_AddedParts", _AddPartsViewModel);
        }
        private List<AddPartViewModel> GetOrderedParts(int OrderId)
        {
            var orderedParts = orderService.GetOrderedParts(OrderId);
            if (orderedParts != null)
                return orderedParts.Select(s => new AddPartViewModel() { Id = s.Id, Name = s.Part.Name, Quantity = s.Quantity, Amount = s.Part.Amount }).ToList();
            else
                return new List<AddPartViewModel>();
        }


        public ActionResult getOrdersByOrderIdJSON(string txt)
        {
            string json = JsonConvert.SerializeObject(orderService.GetOrdersByOrderId(txt, Settings.AutoSuggestPageSize));

            return Json(new { Result = json }, JsonRequestBehavior.AllowGet);
        }












    }
}