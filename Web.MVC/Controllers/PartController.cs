﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.MVC.Controllers
{
    public class PartController : Controller
    {
        private readonly IPartService partService;
        public PartController(IPartService _partService)
        {
            partService = _partService;
        }

        public ActionResult getPartsByNameJSON(string txt)
        {
            string json = JsonConvert.SerializeObject(partService.GetPartsByName(txt, Settings.AutoSuggestPageSize));

            return Json(new { Result = json }, JsonRequestBehavior.AllowGet);
        }
    }
}