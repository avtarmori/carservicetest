﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.MVC.Controllers
{
    public class ModelController : Controller
    {
        private readonly IModelService modelService;
        public ModelController(IModelService _modelService)
        {
            modelService = _modelService;
        }

        public ActionResult getModelByFullNameJSON(string txt, int? BrandId)
        {
            string json = JsonConvert.SerializeObject(modelService.GetModelByFullName(txt, BrandId, Settings.AutoSuggestPageSize), Formatting.Indented,
                                                        new JsonSerializerSettings
                                                        {
                                                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                                        });

            return Json(new { Result = json }, JsonRequestBehavior.AllowGet);
        }
    }
}