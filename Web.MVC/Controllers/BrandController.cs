﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.MVC.Controllers
{
    public class BrandController : Controller
    {
        private readonly IBrandService brandService;
        public BrandController(IBrandService _brandService)
        {
            brandService = _brandService;
        }

        public ActionResult getBrandsByNameJSON(string txt)
        {
            string json = JsonConvert.SerializeObject(brandService.GetBrandsByName(txt, Settings.AutoSuggestPageSize));

            return Json(new { Result = json }, JsonRequestBehavior.AllowGet);
        }
    }
}