﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.MVC.Controllers
{
    public class CarController : Controller
    {
        private readonly ICarService carService;
        public CarController(ICarService _carService)
        {
            carService = _carService;
        }
        public ActionResult getCarsByRegistrationNumberJSON(string txt,int? CustomerId)
        {

            string json = JsonConvert.SerializeObject(carService.GetByRegistrationNumber(txt, CustomerId, Settings.AutoSuggestPageSize), Formatting.Indented,
                                                        new JsonSerializerSettings
                                                        {
                                                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                                        });

            return Json(new { Result = json }, JsonRequestBehavior.AllowGet);
        }
    }
}