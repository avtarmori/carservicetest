﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.MVC.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IServiceService serviceService;
        public ServiceController(IServiceService _serviceService)
        {
            serviceService = _serviceService;
        }

        public ActionResult getServicesByNameJSON(string txt)
        {
            string json = JsonConvert.SerializeObject(serviceService.GetServicesByName(txt, Settings.AutoSuggestPageSize));

            return Json(new { Result = json }, JsonRequestBehavior.AllowGet);
        }
    }
}