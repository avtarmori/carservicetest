﻿using Newtonsoft.Json;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.MVC.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;
        public CustomerController(ICustomerService _customerService)
        {
            customerService = _customerService;
        }
            public ActionResult getCustomersByNameOrMobileNumberJSON(string txt)
        {
            string json = JsonConvert.SerializeObject(customerService.GetCustomersByNameOrMobileNumber(txt, Settings.AutoSuggestPageSize));

            return Json(new { Result = json }, JsonRequestBehavior.AllowGet);
        }
    }
}