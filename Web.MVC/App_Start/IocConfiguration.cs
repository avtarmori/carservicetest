﻿using Autofac;
using Autofac.Integration.Mvc;
using Domain.Interfaces;
using Infrastructure.Data;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.MVC
{
    public static class IocConfiguration
    {
        public static void ConfigureDependencyInjection()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerRequest();

            builder.RegisterType<CarServiceContext>().AsSelf().InstancePerRequest();



            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterAssemblyTypes(typeof(OrderService).Assembly)
             .Where(t => t.Name.EndsWith("Service"))
             .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(OrderRepository).Assembly)
            .Where(t => t.Name.EndsWith("Repository"))
            .AsImplementedInterfaces();
          //  builder.RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies()).Where(t => t.Name.EndsWith("Service") || t.Name.EndsWith("Repository")).AsImplementedInterfaces();
            // builder.RegisterGeneric(AppDomain.CurrentDomain.GetAssemblies().Single(a => a.ExportedTypes.Any(e => e.FullName == "Infrastructure.Data.Repository`1")).GetType()).As(AppDomain.CurrentDomain.GetAssemblies().Single(a => a.ExportedTypes.Any(e => e.FullName == "Domain.Interfaces.IRepository`1")).GetType());

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}