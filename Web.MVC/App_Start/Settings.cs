﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Web.MVC
{
    public static class Settings
    {
        public static void Load()
        {
            DefaultPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultPageSize"]);
            AutoSuggestPageSize = Convert.ToInt32(ConfigurationManager.AppSettings["AutoSuggestPageSize"]);
        }
       
        public static int DefaultPageSize { get; private set; }
        public static int AutoSuggestPageSize { get; private set; }
    }

    
}