﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.MVC.Models
{
    public class AddPartsViewModel
    {
        public int? SelectedPartId { get; set; }
        public int? DeleteOrderedPartId { get; set; }
        public List<AddPartViewModel> AddedParts { get; set; }


    }
}