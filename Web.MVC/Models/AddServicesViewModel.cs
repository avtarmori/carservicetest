﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.MVC.Models
{
    public class AddServicesViewModel
    {
        public int? SelectedServiceId { get; set; }
        public int? DeleteOrderedServiceId { get; set; }
        public List<AddServiceViewModel> AddedServices { get; set; }


    }
}