﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.MVC.Models
{
    public class CreateOrderViewModel
    {
        //Customer Details
        public int? CustomerId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        
        public decimal PhoneNumber { get; set; }
        [EmailAddress]
        public string EmailId { get; set; }

        //Car Details
        public int? CarId { get; set; }
        public int ModelId { get; set; }
        [Required]
        public string BrandName { get; set; }
        [Required]
        public string ModelFullName { get; set; }

        [Required]
        public string RegistrationNumber { get; set; }
        [Required]
        public string VehicleIdentificationNumber { get; set; }

        //Inspection Details
      [Range(0,9999999)]
        public int OdometerReading { get; set; }
         [Range(0,100)]
        public short FuelPercent { get; set; }
        public string CustomerComment { get; set; }
        [Required]
        public string OrderedServicesIds { get; set; }
        [Required]
        public string OrderedPartsIds { get; set; }
    }
}