﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.MVC.Models
{
    public class AddServiceViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Range(1,99,ErrorMessage ="Quanity must be between 1 to 99")]
        public int Quantity { get; set; }
        public int Amount { get; set; }
    }
}