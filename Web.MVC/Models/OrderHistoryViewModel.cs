﻿using Domain.Entities;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.MVC.Models
{
    public class OrderHistoryViewModel
    {
        public int? OrderId { get; set; }
        public int? CustomerId { get; set; }
        public int? CarId { get; set; }
        public IPagedList<Order> Orders { get; set; }

    }
}