﻿$(function () {

    var $inputOrderNumber = $('#OrderId').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            orderObjectsGlobalStorage = new Object();

            $.ajax({
                url: '/Order/getOrdersByOrderIdJSON',
                data: 'txt=' + query,
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (cust) {
                        var text = cust.Id.toString();
                        DispList.push(text);
                        orderObjectsGlobalStorage[text] = cust;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var cust = orderObjectsGlobalStorage[item];

            // Set the text to our selected customer
           // $("#OrderId").val(cust.Id);
            $('form').submit();

            return item;
        },
        highlighter: function (item) {
            var order = orderObjectsGlobalStorage[item];
            var displayText = order.Id;
            return displayText;
        }
    });
    $inputOrderNumber.change(function () {
        var current = $inputOrderNumber.typeahead("getActive");
        if (current) {
            //console.log(current);
            // console.log(current.name);
            // Some item from your model is active!
            if (current == $inputOrderNumber.val()) {


                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
                $("#OrderId").val('');
              
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            $("#OrderId").val('');

        }
    });


    var $inputCustomerNameOrMobileNumber = $('#PhoneNumber').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            customerObjectsGlobalStorage = new Object();

            $.ajax({
                url: '/Customer/getCustomersByNameOrMobileNumberJSON',

                data: 'txt=' + query,
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (cust) {
                        var text = cust.FirstName + " " + cust.LastName + " " + cust.PhoneNumber ;
                        DispList.push(text);
                        customerObjectsGlobalStorage[text] = cust;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var cust = customerObjectsGlobalStorage[item];

            // Set the text to our selected customer
            $("#CustomerId").val(cust.Id);
            $('#PhoneNumber').val(cust.FirstName + ' '+cust.LastName + ' '+cust.PhoneNumber)

            $('form').submit();

            return item;
        },
        highlighter: function (item) {
            var cust = customerObjectsGlobalStorage[item];
            var displayText = cust.FirstName + " " + cust.LastName + " <i>" + cust.PhoneNumber +"</i>";
            return displayText;
        }
    });
    $inputCustomerNameOrMobileNumber.change(function () {
        var current = $inputCustomerNameOrMobileNumber.typeahead("getActive");
        if (current) {
            //console.log(current);
            // console.log(current.name);
            // Some item from your model is active!
            if (current == $inputCustomerNameOrMobileNumber.val()) {

                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
                $("#CustomerId").val('');
                $('#PhoneNumber').val('');
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            $("#CustomerId").val('');
            $('#PhoneNumber').val('');
        }
    });

    var $inputRegistrationNumber = $('#RegistrationNumber').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            carObjectsGlobalStorage = new Object();

            $.ajax({
                url: '/Car/getCarsByRegistrationNumberJSON',


                data: 'txt=' + query,
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (car) {
                        DispList.push(car.RegistrationNumber);
                        carObjectsGlobalStorage[car.RegistrationNumber] = car;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var car = carObjectsGlobalStorage[item];

            $("#CarId").val(car.Id);
            $('#RegistrationNumber').val(car.RegistrationNumber);
            $('form').submit();

            return item;
        },
        highlighter: function (item) {
            var car = carObjectsGlobalStorage[item];
            var displayText = car.RegistrationNumber + " (" + car.Model.Brand.Name + " " + car.Model.Name + ")";
            return displayText;
        }
    });
    $inputRegistrationNumber.change(function () {
        var current = $inputRegistrationNumber.typeahead("getActive");
        if (current) {
            //console.log(current);
            // console.log(current.name);
            // Some item from your model is active!
            if (current == $inputRegistrationNumber.val()) {

                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
                $("#CarId").val('');
                $("#RegistrationNumber").val('');
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            $("#CarId").val('');
            $("#RegistrationNumber").val('');
        }
    });

});