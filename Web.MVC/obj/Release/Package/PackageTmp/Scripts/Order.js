﻿

//var customerNames = new Array();
//var customerIds = new Object();


$(function () { 


    //Create Order 
    var $inputPhoneNumber =  $('#PhoneNumber').typeahead({
        source: function (query, process) {

           var DispList = new Array();
           customerObjectsGlobalStorage = new Object();

            $.ajax({
                url: '/Customer/getCustomersByNameOrMobileNumberJSON',


                data: 'txt=' + query,
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (cust) {
                        var text = cust.PhoneNumber.toString();
                        DispList.push(text);
                        customerObjectsGlobalStorage[text] = cust;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var cust = customerObjectsGlobalStorage[item];

            // Set the text to our selected customer
            $("#CustomerId").val(cust.Id);
            $("#FirstName").val(cust.FirstName);
            $("#LastName").val(cust.LastName);
            $("#EmailId").val(cust.EmailId);

            $("#FirstName").prop('disabled', true);
            $("#LastName").prop('disabled', true);
            $("#EmailId").prop('disabled', true);


            return item;
        },
        highlighter: function (item) {
            var cust = customerObjectsGlobalStorage[item];
            var displayText = cust.PhoneNumber + " (" + cust.FirstName + " " + cust.LastName + ")";
            return displayText;
        }
    });
    $inputPhoneNumber.change(function () {
        var current = $inputPhoneNumber.typeahead("getActive");
        if (current) {
            //console.log(current);
           // console.log(current.name);
            // Some item from your model is active!
            if (current == $inputPhoneNumber.val()) {

              
                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
                $("#CustomerId").val('');

                $("#FirstName").prop('disabled', false);
                $("#LastName").prop('disabled', false);
                $("#EmailId").prop('disabled', false);
            
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            $("#CustomerId").val('');
         
            $("#FirstName").prop('disabled', false);
            $("#LastName").prop('disabled', false);
            $("#EmailId").prop('disabled', false);
        }
    });

    var $inputRegistrationNumber =   $('#RegistrationNumber').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            carObjectsGlobalStorage = new Object();

            var custId = $('#CustomerId').val();
            if (custId == '')
                custId = -1;
            $.ajax({
                url: '/Car/getCarsByRegistrationNumberJSON',
                data: 'txt=' + query + '&CustomerId=' + custId,
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (car) {
                        DispList.push(car.RegistrationNumber);
                        carObjectsGlobalStorage[car.RegistrationNumber] = car;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var car = carObjectsGlobalStorage[item];

          
            $("#CarId").val(car.Id);
            $("#VehicleIdentificationNumber").val(car.VehicleIdentificationNumber);
            $("#BrandName").val(car.Model.Brand.Name);
            $("#BrandId").val(car.Model.Brand.Id);
              //  $('#ModelFullName').typeahead('val', brand.Model.Name);
            $("#ModelFullName").val(car.Model.FullName);
            $("#ModelId").val(car.ModelId);

            $("#VehicleIdentificationNumber").prop('disabled', true);
            $("#BrandName").prop('disabled', true);
            $("#ModelFullName").prop('disabled', true);

            return item;
        },
        highlighter: function (item) {
            var car = carObjectsGlobalStorage[item];
            var displayText = car.RegistrationNumber + " (" + car.Model.Brand.Name + " " + car.Model.Name + ")";
            return displayText;
        }
    });
    $inputRegistrationNumber.change(function () {
        var current = $inputRegistrationNumber.typeahead("getActive");
        if (current) {
            //console.log(current);
            // console.log(current.name);
            // Some item from your model is active!
            if (current == $inputRegistrationNumber.val()) {

                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
                $("#CarId").val('');

                $("#VehicleIdentificationNumber").prop('disabled', false);
                $("#BrandName").prop('disabled', false);
                $("#ModelFullName").prop('disabled', false);

               // $("#VehicleIdentificationNumber").val('');
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            $("#CarId").val('');

            $("#VehicleIdentificationNumber").prop('disabled', false);
            $("#BrandName").prop('disabled', false);
            $("#ModelFullName").prop('disabled', false);

           // $("#VehicleIdentificationNumber").val('');
        }
    });

  

    var $inputBrandName =   $('#BrandName').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            brandObjectsGlobalStorage = new Object();

            $.ajax({
                url: '/Brand/getBrandsByNameJSON',


                data: 'txt=' + query,
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (brand) {
                        DispList.push(brand.Name);
                        brandObjectsGlobalStorage[brand.Name] = brand;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var brand = brandObjectsGlobalStorage[item];
            $("#BrandId").val(brand.Id);

            $("#ModelFullName").val('');
            $("#ModelId").val('');

            return item;
        }
    });
    $inputBrandName.change(function () {
        var current = $inputBrandName.typeahead("getActive");
        if (current) {
            //console.log(current);
            // console.log(current.name);
            // Some item from your model is active!
            if (current == $inputBrandName.val()) {

                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
                $("#BrandId").val('');
                $('#BrandName').val('');
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            $("#BrandId").val('');
            $('#BrandName').val('');
        }
    });

    var $inputModelFullName =  $('#ModelFullName').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            modelObjectsGlobalStorage = new Object();

            $.ajax({
                url: '/Model/getModelByFullNameJSON',


                data: 'txt=' + query + '&BrandId=' + $("#BrandId").val(),
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (model) {
                        DispList.push(model.FullName);
                        modelObjectsGlobalStorage[model.FullName] = model;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var model = modelObjectsGlobalStorage[item];
            $("#ModelId").val(model.Id);
            $("#BrandId").val(model.Brand.Id);
            $("#BrandName").val(model.Brand.Name);
            return item;
        }
    });
    $inputModelFullName.change(function () {
        var current = $inputModelFullName.typeahead("getActive");
        if (current) {
            //console.log(current);
            // console.log(current.name);
            // Some item from your model is active!
            if (current == $inputModelFullName.val()) {

                // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
            } else {
                // This means it is only a partial match, you can either add a new item
                // or take the active if you don't want new items
                $("#ModelId").val('');
                $('#ModelFullName').val('');
            }
        } else {
            // Nothing is active so it is a new value (or maybe empty value)
            $("#ModelId").val('');
            $('#ModelFullName').val('');
        }
    });


    //Add Services to Order
    InitializeServiceNameAutoSuggest();

    //Add Parts to Order
    InitializePartNameAutoSuggest();
});



var currentRequest = null;
 //Add Services to Order
function InitializeServiceNameAutoSuggest() {

    $('#ServiceName').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            serviceObjectsGlobalStorage = new Object();

            currentRequest = $.ajax({
                url: '/Service/getServicesByNameJSON',


                data: 'txt=' + query,
                beforeSend: function () {

                    if (currentRequest != null) {
                        //    alert('abort');
                        currentRequest.abort();
                    }

                },
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (service) {
                        var text = service.Name.toString();
                        DispList.push(text);
                        serviceObjectsGlobalStorage[text] = service;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var service = serviceObjectsGlobalStorage[item];


            $("#SelectedServiceId").val(service.Id);

            // $('form').attr('action', $('form').attr('action') + '?OrderId=' + getUrlVars()['OrderId']).submit();
            $('form').submit();


            return item;
        },
        highlighter: function (item) {
            var service = serviceObjectsGlobalStorage[item];
            var displayText = service.Name;
            return displayText;
        }
    });
}

//Add Services to Order
function InitializePartNameAutoSuggest() {

   
    $('#PartName').typeahead({
        source: function (query, process) {

            var DispList = new Array();
            partObjectsGlobalStorage = new Object();

            currentRequest = $.ajax({
                url: '/Part/getPartsByNameJSON',


                data: 'txt=' + query,
                beforeSend: function () {

                    if (currentRequest != null) {
                        //    alert('abort');
                        currentRequest.abort();
                    }

                },
                success: function (data) {

                    var obj = jQuery.parseJSON(data.Result);

                    obj.map(function (part) {
                        var text = part.Name.toString();
                        DispList.push(text);
                        partObjectsGlobalStorage[text] = part;
                    });
                    //  console.log(data);
                    process(DispList);
                }
            });
        },
        updater: function (item) {
            var part = partObjectsGlobalStorage[item];


            $("#SelectedPartId").val(part.Id);

            // $('form').attr('action', $('form').attr('action') + '?OrderId=' + getUrlVars()['OrderId']).submit();
            $('form').submit();


            return item;
        },
        highlighter: function (item) {
            var part = partObjectsGlobalStorage[item];
            var displayText = part.Name;
            return displayText;
        }
    });
}



