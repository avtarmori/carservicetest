﻿
using Domain.Entities;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Infrastructure.Data
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        internal readonly CarServiceContext context;
        internal DbSet<T> entities;
        string errorMessage = string.Empty;

        public Repository(CarServiceContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }
        public virtual IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }

        public virtual T Get(long id)
        {
            return entities.SingleOrDefault(s => s.Id == id);
        }
        public virtual int Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            context.SaveChanges();

            return entity.Id;
        }



        public virtual void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public virtual void Delete(long id)
        {
            var entity = entities.SingleOrDefault(s => s.Id == id);

            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
            context.SaveChanges();
        }



        public virtual int UOW_Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
           

            return entity.Id;
        }



        public virtual void UOW_Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
          
        }

        public virtual void UOW_Delete(long id)
        {
            var entity = entities.SingleOrDefault(s => s.Id == id);

            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Remove(entity);
          
        }







        public virtual IEnumerable<T> Get(
           Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "")
        {
            int total;
            return GetPage(out total, null, null, filter, orderBy, includeProperties);

        }
        public virtual IEnumerable<T> GetPage(out int totalCount, int? page = null, int? itemsPerPage = null,
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            totalCount = 0;
            page = (page ?? 1) - 1;

            IQueryable<T> query = entities;

            if (filter != null)
                query = query.Where(filter);


            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                query = query.Include(includeProperty);


            if (orderBy != null)
                query = orderBy(query);

            totalCount = query.Count();

            if (itemsPerPage.HasValue)
                query = query.Skip(itemsPerPage.Value * page.Value).Take(itemsPerPage.Value);


            return query.AsEnumerable();
        }


    }
}
