﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
  public  class UnitOfWork : IUnitOfWork
    {
        private readonly CarServiceContext _context;
        private bool _disposed = false;

        public UnitOfWork(CarServiceContext context)
        {
            _context = context;
        }

        public void Commit()
        {
            if (_disposed)
            {
                throw new ObjectDisposedException(this.GetType().FullName);
            }

            _context.SaveChanges();
        }

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}

        //protected virtual void Dispose(bool disposing)
        //{
        //    if (_disposed) return;

        //    if (disposing && _context != null)
        //    {
        //        _context.Dispose();
        //    }

        //    _disposed = true;
        //}
    }
}
