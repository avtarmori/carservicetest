﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace Infrastructure.Data
{
    public class CarServiceContext:DbContext
    {
        public CarServiceContext()
           : base("name=CarServiceContextConnectionString")
       {
            var a = Database.Connection.ConnectionString;

            Database.SetInitializer(new CarInitializeDB());
           
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().Property(t => t.FirstName).HasMaxLength(150);
            modelBuilder.Entity<Customer>().Property(t => t.LastName).HasMaxLength(150);
           
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Service> Services { get; set; }
    }
}
