﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;

namespace Infrastructure.Data
{
    public class CarInitializeDB : CreateDatabaseIfNotExists<CarServiceContext>
    {
        protected override void Seed(CarServiceContext context)
        {
            //Add Brands with its different models.
            context.Brands.Add(
            new Brand()
            {
                Name = "Hyundai",
                CreatedOn = DateTime.Now,
                Models = new List<Model>()
                {
                    new Model() {
                                      Name = "Accent",
                                      AC=true,
                                      AutoTransmission=false,
                                      CC=1200,
                                      Year=2010,
                                      FullName="Accent 1200cc Manual AC 2010"
                                 },
                     new Model() {
                                      Name = "Accent",
                                      AC=true,
                                      AutoTransmission=true,
                                      CC=2200,
                                      Year=2012,
                                      FullName="Accent 2200cc Automatic AC 2012"
                                 },
                     new Model() {
                                      Name = "Eon",
                                      AC=false,
                                      AutoTransmission=true,
                                      CC=1100,
                                      Year=2015,
                                      FullName="Eon 1100cc Automatic Non-AC 2015"
                                 }
                 }

            });
            context.Brands.Add(
           new Brand()
           {
               Name = "Toyota",
               CreatedOn = DateTime.Now,
               Models = new List<Model>()
                {
                    new Model() {
                                      Name = "Innova",
                                      AC=true,
                                      AutoTransmission=false,
                                      CC=1200,
                                      Year=2010,
                                      FullName="Innova 1200cc Manual AC 2010"
                                 },
                     new Model() {
                                      Name = "Innova",
                                      AC=false,
                                      AutoTransmission=true,
                                      CC=1800,
                                      Year=2010,
                                      FullName="Innova 1800cc Automatic Non-AC 2010"
                                 }
                 }

           });
            context.Brands.Add(
            new Brand()
            {
                Name = "Audi",
                CreatedOn = DateTime.Now,
                Models = new List<Model>()
                {
                    new Model() {
                                      Name = "A1",
                                      AC=true,
                                      AutoTransmission=false,
                                      CC=1200,
                                      Year=2010,
                                      FullName="A1 1200cc Manual AC 2010"
                                 },
                     new Model() {
                                      Name = "A7",
                                      AC=false,
                                      AutoTransmission=true,
                                      CC=1800,
                                      Year=2010,
                                      FullName="A7 1800cc Automatic Non-AC 2010"
                                 }
                 }

            });

            //Add Services
            context.Services.Add(
                new Service()
                {
                    Name = "Engine Oil Change",
                    Amount = 250,
                    TaxPercentage = 10
                });
            context.Services.Add(
             new Service()
             {
                 Name = "Brake Oil Change",
                 Amount = 150,
                 TaxPercentage = 10
             });
            context.Services.Add(
             new Service()
             {
                 Name = "Wheel Alignment",
                 Amount = 450,
                 TaxPercentage = 10
             });

            //Add Parts
            context.Parts.Add(
                new Part()
                {
                    Name="Shell Oil 2L",
                    Amount=500,
                    TaxPercentage=15
                });
            context.Parts.Add(
              new Part()
              {
                  Name = "Castrol Oil 5L",
                  Amount = 1000,
                  TaxPercentage = 15
              });
            context.Parts.Add(
             new Part()
             {
                 Name = "Air Filter",
                 Amount = 80,
                 TaxPercentage = 5
             });


            context.SaveChanges();
            base.Seed(context);
        }
    }
}
