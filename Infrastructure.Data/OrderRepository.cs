﻿using Domain.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;


namespace Infrastructure.Data
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {

        private readonly CarServiceContext context;
        public OrderRepository(CarServiceContext context) : base(context)
        {
            this.context = context;
        }

        public List<OrderedPart> GetOrderedParts(int OrderId)
        {
            return context.Orders.Include(parent => parent.OrderedParts.Select(child => child.Part)).Single(o => o.Id == OrderId).OrderedParts;
        }

        public List<OrderedService> GetOrderedServices(int OrderId)
        {
            return context.Orders.Include(parent => parent.OrderedServices.Select(child => child.Service)).Single(o => o.Id == OrderId).OrderedServices;
        }

        
    }
}
