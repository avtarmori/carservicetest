﻿using Services.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using Domain.Interfaces;
using System.Linq;
using Infrastructure.DataFilters;

namespace Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Customer> _customerGenericRepository;
        private readonly IRepository<Car> _carGenericRepository;
        private readonly IRepository<Service> _serviceGenericRepository;
        private readonly IRepository<Part> _partGenericRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IRepository<OrderedService> _orderedServiceGenericRepository;
        private readonly IRepository<OrderedPart> _orderedPartGenericRepository;
        private readonly IUnitOfWork _unitOfWork;
        public OrderService(
           IRepository<Customer> customerGenericRepository
            , IRepository<Car> carGenericRepository
            , IRepository<Service> serviceGenericRepository
            , IRepository<Part> partGenericRepository
            , IOrderRepository orderRepository
            , IRepository<OrderedService> orderedServiceGenericRepository
            , IRepository<OrderedPart> orderedPartGenericRepository
            , IUnitOfWork unitOfWork
            )
        {
            // _orderGenericRepository = orderGenericRepository;
            _customerGenericRepository = customerGenericRepository;
            _carGenericRepository = carGenericRepository;
            _serviceGenericRepository = serviceGenericRepository;
            _partGenericRepository = partGenericRepository;

            _orderRepository = orderRepository;

            _orderedServiceGenericRepository = orderedServiceGenericRepository;

            _orderedPartGenericRepository = orderedPartGenericRepository;

            _unitOfWork = unitOfWork;
        }

        public void AddService(int ServiceId, int OrderId)
        {

           
                var order = _orderRepository.Get(OrderId);
                var service = _serviceGenericRepository.Get(ServiceId);

                var orderedService = new OrderedService() { OrderId = OrderId, ServiceId = ServiceId, Quantity = 1 };
                _orderedServiceGenericRepository.UOW_Insert(orderedService);

                order.Amount += service.Amount * orderedService.Quantity;
                _orderRepository.UOW_Update(order);

                _unitOfWork.Commit();
            
        }

        public void RemoveService(int OrderedServiceId)
        {


           
                var orderedService = _orderedServiceGenericRepository.Get(OrderedServiceId);

                var order = _orderRepository.Get(orderedService.OrderId);
                var service = _serviceGenericRepository.Get(orderedService.ServiceId);

                _orderedServiceGenericRepository.UOW_Delete(OrderedServiceId);

                order.Amount -= service.Amount;
                _orderRepository.UOW_Update(order);

                _unitOfWork.Commit();
            
        }

        public void UpdateServices(IEnumerable<OrderedService> orderedService)
        {
            if (orderedService.Count() > 0)
            {

                var _os = _orderedServiceGenericRepository.Get(orderedService.First().Id);
                var order = _orderRepository.Get(_os.OrderId);

                    foreach (OrderedService os in orderedService)
                    {
                        var service = _serviceGenericRepository.Get(_orderedServiceGenericRepository.Get(os.Id).ServiceId);

                         _os = _orderedServiceGenericRepository.Get(os.Id);
                        if (_os.Quantity != os.Quantity)
                        {
                            int quantityChange = os.Quantity - _os.Quantity;
                            if (quantityChange != 0)
                            {
                                  _os.Quantity = os.Quantity;
                                _orderedServiceGenericRepository.UOW_Update(_os);

                                order.Amount += quantityChange * service.Amount;
                               
                            }

                        }
                      
                    }
                _orderRepository.UOW_Update(order);
                _unitOfWork.Commit();
            }
        }


        public void AddPart(int PartId, int OrderId)
        {
                var order = _orderRepository.Get(OrderId);
                var part = _partGenericRepository.Get(PartId);

                var orderedPart = new OrderedPart() { OrderId = OrderId, PartId = PartId, Quantity = 1 };
                _orderedPartGenericRepository.UOW_Insert(orderedPart);

                order.Amount += part.Amount * orderedPart.Quantity;
                _orderRepository.UOW_Update(order);

                _unitOfWork.Commit();
           
        }

        public void RemovePart(int OrderedPartId)
        {
                var orderedPart = _orderedPartGenericRepository.Get(OrderedPartId);

                var order = _orderRepository.Get(orderedPart.OrderId);
                var part = _partGenericRepository.Get(orderedPart.PartId);

                _orderedPartGenericRepository.UOW_Delete(OrderedPartId);

                order.Amount -= part.Amount;
                _orderRepository.UOW_Update(order);

                _unitOfWork.Commit();
            
        }

        public void UpdateParts(IEnumerable<OrderedPart> orderedPart)
        {
            if (orderedPart.Count() > 0)
            {
                var _os = _orderedPartGenericRepository.Get(orderedPart.First().Id);
                var order = _orderRepository.Get(_os.OrderId);

                    foreach (OrderedPart os in orderedPart)
                    {
                        var part = _partGenericRepository.Get(_orderedPartGenericRepository.Get(os.Id).PartId);

                         _os = _orderedPartGenericRepository.Get(os.Id);
                        if (_os.Quantity != os.Quantity)
                        {
                            int quantityChange = os.Quantity - _os.Quantity;
                            if (quantityChange != 0)
                            {
                                _os.Quantity = os.Quantity;
                                _orderedPartGenericRepository.UOW_Update(_os);
                                order.Amount += quantityChange * part.Amount;
                               
                            }

                        }
                       
                    }
                _orderRepository.UOW_Update(order);
                _unitOfWork.Commit();
            }
        }


     
        public int Create(Order order)
        {
            if (order.Car != null)
            {
                if (order.Customer != null)
                    order.Customer.CustomerCars = new List<CustomerCars>() { new CustomerCars() { Car = order.Car } };
                else
                {
                    order.Car.CustomerCars = new List<CustomerCars>() { new CustomerCars() { CustomerId = order.CustomerId } };
                }
            }

            return _orderRepository.Insert(order);
        }





        public IEnumerable<Order> GetOrderPage(int? page, int itemsPerPage, OrderFilter orderFilter, out int totalCount)
        {
            //return _orderRepository.GetOrderPage(page, itemsPerPage,new Domain.Interfaces.Filters.OrderFilter() {CarId=orderFilter.CarId, CustomerId=orderFilter.CustomerId, OrderId=orderFilter.OrderId }, out totalCount);
            return _orderRepository.GetPage(out totalCount, page, itemsPerPage, of => (orderFilter.CarId == null || of.CarId == orderFilter.CarId) && (orderFilter.CustomerId == null || of.CustomerId == orderFilter.CustomerId) && (orderFilter.OrderId == null || of.Id == orderFilter.OrderId), ob => ob.OrderByDescending(obi => obi.CreatedOn), "Customer,Car");
        }

        public List<OrderedService> GetOrderedServices(int OrderId)
        {
            return _orderRepository.GetOrderedServices(OrderId);
        }
        public List<OrderedPart> GetOrderedParts(int OrderId)
        {
            return _orderRepository.GetOrderedParts(OrderId);
        }

        public IEnumerable<Order> GetOrdersByOrderId(string OrderId, int? PageSize = null)
        {
            int total;
            return _orderRepository.GetPage(out total, null,  PageSize, o => o.Id.ToString().StartsWith(OrderId), or => or.OrderBy(ord => ord.Id));
        }


    }
}
