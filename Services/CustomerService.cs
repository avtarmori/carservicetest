﻿using Services.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using Domain.Interfaces;
using System.Linq;

namespace Services
{
    public class CustomerService : ICustomerService
    {
       
        private readonly IRepository<Customer> _customerRepository;

        public CustomerService(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;

        }
        public IEnumerable<Customer> GetCustomersByNameOrMobileNumber(string PhoneNumberAndOrName, int? PageSize = null)
        {
            int total;
            return _customerRepository.GetPage(out total,null,PageSize, c => (c.FirstName + " " + c.LastName + " " + c.PhoneNumber).ToString().StartsWith(PhoneNumberAndOrName) || c.PhoneNumber.ToString().StartsWith(PhoneNumberAndOrName),ob=>ob.OrderBy(ord => ord.FirstName + " " + ord.LastName + " " + ord.PhoneNumber));
        }
     
    }
}
