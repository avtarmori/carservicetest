﻿using Services.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using Domain.Interfaces;
using System.Linq;

namespace Services
{
    public class CarService : ICarService
    {
       
       
        private readonly IRepository<Car> _carRepository;
       
        public CarService(IRepository<Car> carRepository)
        {
          
            _carRepository = carRepository;

        }
     
        public IEnumerable<Car> GetByRegistrationNumber(string RegistrationNumber,int? CustomerId, int? PageSize = null)
        {
            int total;
            return _carRepository.GetPage(out total, null, PageSize, b => b.RegistrationNumber.StartsWith(RegistrationNumber) && (CustomerId==null || b.CustomerCars.Any(c=>c.CustomerId==CustomerId)), o => o.OrderBy(ob => ob.RegistrationNumber),"Model.Brand");
        }

     
    }
}
