﻿using Services.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using Domain.Interfaces;
using System.Linq;

namespace Services
{
    public class BrandService : IBrandService
    {
        private readonly IRepository<Brand> _brandRepository;

        public BrandService(IRepository<Brand> brandRepository)
        {
            _brandRepository = brandRepository;
        }
      
        public IEnumerable<Brand> GetBrandsByName(string Name, int? PageSize = null)
        {
            int total;
            return _brandRepository.GetPage(out total,null, PageSize, b=>b.Name.StartsWith(Name),o=>o.OrderBy(ob=>ob.Name));
        }
    }
}
