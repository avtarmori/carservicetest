﻿using Services.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using Domain.Interfaces;
using System.Linq;

namespace Services
{
    public class PartService : IPartService
    {
        private readonly IRepository<Part> _partRepository;
        public PartService(IRepository<Part> partRepository)
        {
            _partRepository = partRepository;
        }

        public IEnumerable<Part> GetPartsByName(string PartName, int? PageSize = null)
        {
            int total;
            return _partRepository.GetPage(out total, null, PageSize, c => c.Name.Contains(PartName),or=>or.OrderBy(ord => ord.Name));
        }
     
    }
}
