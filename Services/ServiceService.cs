﻿using Services.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using Domain.Interfaces;
using System.Linq;

namespace Services
{
    public class ServiceService : IServiceService
    {
        private readonly IRepository<Service> _serviceRepository;
       
        public ServiceService(IRepository<Service> serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }

        public IEnumerable<Service> GetServicesByName(string ServiceName, int? PageSize=null)
        {
            int total;
            return _serviceRepository.GetPage(out total,null, PageSize, c => c.Name.Contains(ServiceName),or=>or.OrderBy(ord => ord.Name));
        }
     
    }
}
