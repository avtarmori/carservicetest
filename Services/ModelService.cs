﻿using Services.Interfaces;
using System;
using Domain.Entities;
using System.Collections.Generic;
using Domain.Interfaces;
using System.Linq;

namespace Services
{
    public class ModelService : IModelService
    {
        private readonly IRepository<Model> _modelRepository;

        public ModelService(IRepository<Model> modelRepository)
        {
            _modelRepository = modelRepository;
        }
      
        public IEnumerable<Model> GetModelByFullName(string FullName,int? BrandId, int? PageSize = null)
        {
            int total;
            return _modelRepository.GetPage(out total,null,PageSize, c => c.FullName.ToString().StartsWith(FullName) && (BrandId == null || c.Brand.Id == BrandId),or=>or.OrderBy(ord => ord.FullName), "Brand");
        }
    }
}
