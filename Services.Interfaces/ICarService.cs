﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface ICarService
    {
        IEnumerable<Car> GetByRegistrationNumber(string RegistrationNumber,int? CustomerId, int? PageSize = null);
    }
}
