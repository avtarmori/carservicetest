﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IServiceService
    {


        IEnumerable<Service> GetServicesByName(string ServiceName, int? PageSize = null);
    }
}
