﻿using Domain.Entities;
using Infrastructure.DataFilters;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IOrderService
    {
        int Create(Order order);
    
        IEnumerable<Order> GetOrderPage(int? page, int itemsPerPage, OrderFilter orderFilter, out int totalCount);

        IEnumerable<Order> GetOrdersByOrderId(string OrderId, int? PageSize = null);

        List<OrderedService> GetOrderedServices(int OrderId);
        List<OrderedPart> GetOrderedParts(int OrderId);
        void AddService(int ServiceId,int OrderId);
        void RemoveService(int OrderedServiceId);
        void UpdateServices(IEnumerable<OrderedService> orderedService);
        void AddPart(int PartId, int OrderId);
        void RemovePart(int OrderedPartId);
        void UpdateParts(IEnumerable<OrderedPart> orderedPart);
    }
}
