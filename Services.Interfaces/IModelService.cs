﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IModelService
    {

        IEnumerable<Model> GetModelByFullName(string FullName, int? BrandId, int? PageSize = null);
    }
}
