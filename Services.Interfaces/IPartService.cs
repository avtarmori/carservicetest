﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IPartService
    {


        IEnumerable<Part> GetPartsByName(string ServiceName, int? PageSize = null);
    }
}
