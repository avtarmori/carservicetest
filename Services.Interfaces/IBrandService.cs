﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IBrandService
    {
      

        IEnumerable<Brand> GetBrandsByName(string Name, int? PageSize = null);
    }
}
