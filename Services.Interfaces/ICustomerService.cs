﻿using Domain.Entities;
using System;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetCustomersByNameOrMobileNumber(string PhoneNumberAndOrName, int? PageSize = null);
    }
}
